using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    [Tooltip("Prefab a ser instanciado")]
    public GameObject objPrefab;
    [Tooltip("Container dos objetos")]
    public Transform objParent;
    [Tooltip("Ancora de referencia para o spawn")]
    public Transform spawnAnchor;
    [Tooltip("Altura maxima")]
    public float maxHeight = 2;
    [Tooltip("Tempo entre a cria��o dos objetos")]
    public float timeBetweenSpawn = 1;
    [Tooltip("Tempo para destruir o objeto")]
    public float timeToDestroy = 8;
    float timer = 0;

    void Start()
    {
        SpawnObj();
    }

    void SpawnObj()
    {
        GameObject obj = Instantiate(objPrefab, objParent);
        obj.transform.position = spawnAnchor.position + new Vector3(0, Random.Range(-maxHeight, maxHeight), 0);
        Destroy(obj, timeToDestroy);
    }


    void Update()
    {
        if(timer > timeBetweenSpawn)
        {
            SpawnObj();
            timer = 0;
        }

        timer += Time.deltaTime;
    }
}
