using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour
{
    [Tooltip("Velocidade")]
    public float speed = 2;

    void Update()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
    }
}
