using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScreen : MonoBehaviour
{
    internal void GameOver()
    {
        gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void StartGame()
    {
        gameObject.SetActive(false);
        Score.Instance.ResetPoints();
        Time.timeScale = 1;
    }
}
