using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScreen : MonoBehaviour
{
    public Text scoreText;

    private void Start()
    {
        UpdateScore(0);
    }

    internal void UpdateScore(int points)
    {
        scoreText.text = points.ToString();
    }
}
